# LP1



## Sistema de Ventas

En el Presente proyecto, crearemos un sistema de ventas desktop en C# empleando la arquitectura de N-Capas.

## Arquitectura del sistema

![arquitectura](Arquitectura/n-capas.png)

## Agregando Conexion a la base de datos

Crearemos una clase Conexion.cs dentro del proyecto Sistema.Datos y ahrehamos el siguiente codigo

```c#
.
.
.
public class Conexion
    {
        private string Base;
        private string Servidor;
        private string Usuario;
        private string Clave;
        private bool Seguridad;
        private static Conexion Con = null;

        private Conexion()
        {
            this.Base = "dbsistema";
            this.Servidor = "DESKTOP-Q9A25HI\\SQLEXPRESS";
            this.Usuario = "sa";
            this.Clave = "jucardi7";
            this.Seguridad = true;
        }
        public SqlConnection CrearConexion()
        {
            SqlConnection Cadena = new SqlConnection();
            try
            {
                Cadena.ConnectionString = "Server=" + this.Servidor + "; Database=" + this.Base + ";";
                if (this.Seguridad)
                {
                    Cadena.ConnectionString = Cadena.ConnectionString + "Integrated Security = SSPI";
                }
                else
                {
                    Cadena.ConnectionString = Cadena.ConnectionString + "User Id=" + this.Usuario + ";Password=" + this.Clave;
                }
            }
            catch (Exception ex)
            {
                Cadena = null;
                throw ex;
            }
            return Cadena;
        }

        public static Conexion getInstancia()
        {
            if (Con == null)
            {
                Con = new Conexion();
            }
            return Con;
        }
    }
.
.
.
```

## Agregamos las entidades 

[Entidades](https://drive.google.com/file/d/1piISYclzkjyl4WwYb4lV9bmjSN0T0WCN/view?usp=sharing)


## Agregamos las clases de la capa Datos

[Datos](https://drive.google.com/file/d/1M5OYGAeLqCMXjKmSL2iHfEp6VyXCAvCv/view?usp=sharing)


## Agregamos las clases de la capa Negocio

[Negocio](https://drive.google.com/file/d/1Us6HYKTB9NnnQNCedKUDUb0SOegb6gIy/view?usp=sharing)
